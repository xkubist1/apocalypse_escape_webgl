## Apocalypse escape

*Projekt do předmětu PG2, Jakub Kubišta.*

---

**Námět**

• Hra bude představovat útěk před obřím mimozemšťanem, který se přibližuje směrem k pozorovateli.

• Tato arkáda vznikla na základě hry „The Chase“, ve které je cílem stejně jako v této hře prchat co nejdéle před entitou na pozadí.

• Hráč bude ovládat postavu, která se bude pohybovat v apokalyptickém městě před tímto monstrem rovněž směrem k pozorovateli.

• Na silnici se budou objevovat překážky (bedny), do kterých hráč může narazit.

• *Viz. ukázka podobné hry: [odkaz](https://vimeo.com/62941354).*

---

**Logika**

• Cílem hry je tedy prchat co nejdéle před vlnou a nasbírat co nejvíce bodů, které se přičítají na základě trvání hry.

•  Hráč má tři životy, které může ztratit při interakci s bednou nebo vlnou. Pokud hráč příjde o všechny životy, nastává konec hry.

•  V případě, že hráč ztratí jeden život, tak je na 3 sekundy nesmrtelný, což bude znázorněno blikáním postavy.

---

**Ovládání**

• Běh (S nebo ↓).

• Pohyb doprava (D nebo →).

• Pohyb doleva (A nebo ←).

• Skok (MEZERNÍK).

• Pozorovatel fixní / ovladatelný myší (X).

• Přiblížit (C).

• Oddálit (V).

• Pauza (P).

• Animační test / sebevražda 1 (N).

• Animační test / sebevražda  2 (M).

---

**Spuštění hry**

• *Viz. aktuální verze: [link](https://akela.mendelu.cz/~xkubist1/).*

---
**Poděkování**

• Za rady a vzorové kódy děkuji Jerome Etienne and Davidu Prochazkovi.
